#!/usr/bin/env python3


class WRWord:
    """
    Classe représentant un mot traduit depuis WordReference.
    Il contient :
        - l'URL de recherche
        - le mot en lui-même
        - une liste de traductions courantes
        - une liste de formes composées
    """
    
    def __init__(self, word):
        self.url = ''
        self.word = word
        self.exists = True
        self.mainTranslates = []
        self.compoundForms = []
       
    def print(self):
        
        print("Le mot du jour est: %s" % self.word)
        
        if (self.url):
            print("  --> %s" % self.url)
        
        if not self.exists:
            print("Le mot %s n'existe pas" % self.word)
            return
        
        if self.mainTranslates:
            print("\nTraductions principales:")
            for t in self.mainTranslates:
                t.print()
        
        if self.compoundForms:
            print("\nFormes composées:")
            for t in self.compoundForms:
                t.print()


class WRTranslate:
    """
    Classe représentant une traduction d'un mot traduit depuis WordReference.
    Il contient :
        - le mot traduit (juste un libellé, ou sa forme composée)
        - une note sur l'utilisation à faire
        - une traduction directe
        - une liste d'exemples
    """
    
    def __init__(self):
        self.name = ''
        self.notes = ''
        self.translate = ''
        self.examples = []
    
    def cleanData(self):
        self.name = self.name.strip()
        self.notes = self.notes.strip()
        self.translate = self.translate.strip()
        for ex in self.examples:
            ex = ex.strip()
    
    def print(self):
        print('\n+ %s:\n\t%s' % (self.name, self.translate))
        for ex in self.examples:
            print('--> %s' % ex)
