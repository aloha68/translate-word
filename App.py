#!/usr/bin/env python3

import sys


from WordReferenceClient import WRClient
from IHM import IHM
from Dictionary import Dictionary


class App:
    """
    Classe représentant l'application de traduction de mot.
    """

    def __init__(self, dictionary_file):
        
        self.ihm = IHM()
        self.words = Dictionary(dictionary_file)
        self.firstsLetters = None

        if len(sys.argv) > 1:
            self.firstsLetters = sys.argv[1]

    def get_word(self):
        
        # Si on veut n'importe quel mot
        if not self.firstsLetters:
            return self.words.get_random_word()
            
        # Si on a une préférence pour le début du mot
        try:
            return self.words.get_random_word_starts_with(self.firstsLetters)
        except IndexError:
            print("Aucun mot dans le dictionnaire ne débute par %s" % self.firstsLetters)
            exit()

    def get_translated_word(self, word):
        return WRClient().get_word(word)

    def run(self):

        learnSomething = False
        self.ihm.say_welcome()
        
        while not learnSomething:
            word = self.get_word()
            
            try:
                if self.ihm.ask_question(word):
                    
                    translatedWord = self.get_translated_word(word)
                    if not translatedWord.exists:
                        print("Le mot n'existe pas sur wordreference.com... Désolé !")
                        print("")
                        continue
                    
                    self.ihm.show_word(translatedWord)
                    learnSomething = True
                    
            except KeyboardInterrupt:
                break

        self.ihm.say_goodbye(learnSomething)
