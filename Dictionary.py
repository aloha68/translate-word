#!/usr/bin/env python3

import os
import random


class Dictionary:
    """
    Classe représentant un dictionnaire de mots.
    Le chemin d'entrée correspond à un fichier contenant un mot par ligne.
    """
    
    def __init__(self, path):
        """
        
        """
        if not os.path.isfile(path):
            raise Exception("Le fichier de dictionnaire n'existe pas !")
            
        self.path = path
        self.init_words()

    def init_words(self):
        self.words = open(self.path).read().splitlines()

    def get_random_word(self, deleteWord=False):

        wordFound = False

        while not wordFound:
            
            line = random.choice(self.words)

            if deleteWord:
                self.words.remove(line)

            line = [w for w in line.split('\t') if w]
            
            word = line[0]
            wordFound = True
            
        return word

    def get_random_word_starts_with(self, begin):

        wordFound = False

        while not wordFound:
        
            word = self.get_random_word(deleteWord=True)
            if word.startswith(begin):
                wordFound = True

        self.init_words()
        return word
