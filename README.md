# translate-word

Ce programme permet de rechercher la traduction d'un mot par le biais du site WordReference.

Il s'utilise de deux manières différentes :

### ./random-word

En exécutant le programme sans argument, un mot est tiré au hasard dans le dictionnaire.
L'utilisateur a alors le choix entre accepter ce mot ou en tirer un nouveau au hasard.

    ./random-word.py
    
    Le mot du jour sera-t-il: tranquillité ? [y/N] 
    Le mot du jour sera-t-il: aiguille ? [y/N] 
    Le mot du jour sera-t-il: vilain ? [y/N] y

### ./random-word [pattern]

Dans ce cas, le programme va toujours rechercher un mot au hasard dans le dictionnaire, en s'assurant cette fois qu'il commence par le *pattern* renseigné.

    ./random-word.py gent
    
    Le mot du jour sera-t-il: gentiment ? [y/N] 
    Le mot du jour sera-t-il: gentilhomme ? [y/N] 
    Le mot du jour sera-t-il: gent ? [y/N] 
    Le mot du jour sera-t-il: gentil ? [y/N] y

### Affichage du résultat

Dans tous les cas, lorsque l'utilisateur a validé un mot, le résultat est formaté de la manière suivante :

    Le mot du jour est : gentil

    ➜ gentil (personne : aimable)
	    kind, nice, sweet
    ↳ « Cette petite fille est très gentille. »
    ↳ « That little girl is very sweet. »

