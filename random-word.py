#!/usr/bin/env python3
#
# Requiert les modules pip suivants: requests, prompt_toolkit, pygments
#

import os
from App import App


if __name__ == '__main__':
    
    dict_file = os.path.join(
        os.path.dirname(os.path.realpath(__file__)),
        'dict.txt'
    )
    
    App(dict_file).run()
