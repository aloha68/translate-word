#!/usr/bin/env python3


from prompt_toolkit import prompt
from prompt_toolkit.shortcuts import print_tokens
from prompt_toolkit.styles import style_from_dict
from pygments.token import Token


class IHM:
    """
    Classe permettant la mise en forme de l'application.
    Application console utilisant les modules PIP suivants : prompt_toolkit et pygments
    """
    
    def __init__(self):
        self.currentWord = ''
        self.promptDefault = ''
        
    def get_title(self):
        """Méthode permettant de donner un titre au terminal"""
        return "Viens apprendre un mot avec ton copain Thomas :-)"
        
    def get_prompt_tokens(self, cli):
        """Méthode permettant de récupérer le prompt à composer"""
        return [
            (Token.Question,        "Le mot du jour sera-t-il: "),
            (Token.Word,            self.currentWord),
            (Token.AskMark,         " ? "),
            (Token.DefaultChoice,   "[%s] " % self.promptDefault)
        ]
        
    def get_style(self):
        """Méthode permettant de récupérer le style du prompt à composer"""
        return style_from_dict({
            Token:                  "",
            Token.Question:         "",
            Token.Word:             "bold",
            Token.AskMark:          "",
            Token.DefaultChoice:    "",
            Token.WordOfTheDay:     "bold underline",
            Token.Bullet:           "",
            Token.Notes:            "",
            Token.Translate:        "italic",
            Token.Example:          "",
        })
        
    def get_corrects_answers(self):
        """Retourne la liste des réponses possibles pour une question de type yes/no"""
        return {
            'yes': True, 'y': True, 'oui': True, 'ou': True, 'o': True,
            'non': False, 'no': False, 'n': False
        }
        
    def prompt(self):
        """Méthode permettant d'afficher le prompt et de retourner le résultat"""
        return prompt(
            get_prompt_tokens=self.get_prompt_tokens,
            get_title=self.get_title,
            style=self.get_style()
        )
        
    def ask_question(self, word, default='no'):
        """Pose une question de type yes/no"""
        
        self.currentWord = word
        answers = self.get_corrects_answers()

        self.promptDefault = 'y/n'
        if default in answers:
            if answers[default]:
                self.promptDefault = 'Y/n'
            else:
                self.promptDefault = 'y/N'
        
        answer = self.prompt().strip().lower()
        
        if not answer and default is not None and default in answers:
            return answers[default]
        elif answer in answers:
            return answers[answer]
        else:
            return answer
        
    def say_welcome(self):
        print("")
        print("★ Hëllø, prêt pour apprendre un nouveau mot ? ★")
        print("")
        
    def say_goodbye(self, learnSomething=True):
        print("")
        if not learnSomething:
            print("Dommage, aucun mot appris aujourd'hui ¡¡¡")

        print("✿ À demain pour un nouveau mot ! ✿")
        print("")
        
    def show_word(self, translatedWord):
        """Méthode permettant d'afficher un mot traduit"""
        
        tokens = [
            (Token,                 "\nLe mot du jour est : "),
            (Token.WordOfTheDay,    translatedWord.word),
        ]
        
        print_tokens(tokens, style=self.get_style())
        
        for tr in translatedWord.mainTranslates:
            self.show_translate(tr)
        
        for tr in translatedWord.compoundForms:
            self.show_translate(tr)
        
        print("")
        
    def show_translate(self, translate):
        """Méthode permettant d'afficher une traduction pour le mot en cours"""
        
        tokens = [
            (Token,                 "\n\n"),
            (Token.Bullet,          "➜ "),
            (Token.Word,            translate.name),
            (Token.Notes,           " %s" % translate.notes),
            (Token,                 "\n\t"),
            (Token.Translate,       translate.translate),
        ]
        
        print_tokens(tokens, style=self.get_style())
        
        for ex in translate.examples:
            tokens = [
                (Token,             "\n"),
                (Token.Bullet,      " ↳ "),
                (Token.Example,     "« %s »" % ex),
            ]
            print_tokens(tokens, style=self.get_style())
        
    def show_error(self, error):
        """Méthode permettant d'afficher une erreur"""
        print("")
        print("[ERROR] %s" % error)
        print("")
        
    def run(self):
        print(self.ask_question('élégant'))
