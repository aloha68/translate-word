#!/usr/bin/env python3

import requests
from html.parser import HTMLParser

from WRModels import *
from CustomExceptions import *


class WRHTMLParser(HTMLParser):
    
    def __init__(self, word):
        HTMLParser.__init__(self)
        
        self.word = WRWord(word)
        self.wordExists = True
        self.compoundFormsStarts = False
        
        self.rowIndex = -1
        self.previousRowIndex = -1
        self.dataCurrentRowIndex = -1
        
        self.columnIndex = -1
        self.previousColumnIndex = -1
        
        self.captureExample = False
        self.currentTranslate = None
        
    def get_word(self):
        self.word.exists = self.wordExists
        
        for x in self.word.mainTranslates:
            x.cleanData()
        
        for x in self.word.compoundForms:
            x.cleanData()
            
        return self.word
    
    def getAttr(self, attrs, search):
        value = ''
        for attr in attrs:
            if attr[0] == search:
                value = attr[1]
                break
        return value
    
    def getId(self, attrs):
        return self.getAttr(attrs, 'id')
    
    def createNewTranslate(self):
        self.currentTranslate = WRTranslate()
    
    def addTranslateToWord(self):
        if self.currentTranslate:
            if not self.compoundFormsStarts:
                self.word.mainTranslates.append(self.currentTranslate)
            else:
                self.word.compoundForms.append(self.currentTranslate)
        self.currentTranslate = None
    
    def handle_starttag(self, tag, attrs):

        if not self.wordExists:
            return
            
        tagId = self.getId(attrs)
        tagClass = self.getAttr(attrs, 'class')
        
        # Si le mot n'existe pas
        if tagId == 'noEntryFound':
            self.wordExists = False
            return
        
        # Balises TABLE
        if tag == 'table' and tagId == 'compound_forms':
            self.addTranslateToWord()
            self.compoundFormsStarts = True
        
        # Balises TR
        if tag == 'tr':
            
            self.previousRowIndex = self.rowIndex
            self.rowIndex += 1
            
            # Début de l'écriture 
            if tagId.startswith('fren:'):
                self.columnIndex = 0                
                self.addTranslateToWord()
                #print("Nouvelle traduction !")
                self.createNewTranslate()
            return
        
        # Balises TD
        if tag == 'td' and self.columnIndex >= 0:
            self.columnIndex += 1
            
            if tagClass == 'ToEx' or tagClass == 'FrEx':
                self.captureExample = True
            return
        
        # Balises EM
        if self.columnIndex >= 0 and (tag == 'em' or tag == 'span' and tagClass == 'tooltip'):
            self.previousColumnIndex = self.columnIndex
            self.columnIndex = -1
            return
        
    def handle_endtag(self, tag):
        
        if tag == 'table' and self.columnIndex >= 0:
            self.columnIndex = -1
            self.addTranslateToWord()
            return
            
        if tag == 'tr' and self.columnIndex >= 0:
            self.columnIndex = 0
        
        if self.previousColumnIndex >= 0 and (tag == 'em' or tag == 'span'):
            self.columnIndex = self.previousColumnIndex
            self.previousColumnIndex = -1
    
    def handle_data(self, data):
        
        data = data.strip()
        if not data:
            return
            
        if self.columnIndex >= 0 and self.currentTranslate:
            
            #print('Colonne %s: %s' % (self.columnIndex, data))
            
            # Colonne 1, c'est les mots français
            # Si ça dépasse, c'est la suite du nom pour la traduction en cours
            if self.columnIndex == 1:
                self.currentTranslate.name += '%s ' % data
            
            # Colonne 2, c'est plus compliqué
            elif self.columnIndex == 2:
                
                if data.startswith('(') and data.endswith(')'):
                    self.currentTranslate.notes += '%s' % data
            
            # Colonne 3, la traduction anglaise
            # 
            elif self.columnIndex == 3:
                
                if self.dataCurrentRowIndex < self.rowIndex and self.currentTranslate.translate:
                    data = ', %s' % data
                elif self.dataCurrentRowIndex == self.rowIndex:
                    data = ' %s' % data
                    
                self.currentTranslate.translate += data
                self.dataCurrentRowIndex = self.rowIndex
            
        # Cas particulier des exemples qui ne sont pas dans une colonne
        if self.captureExample:
            self.currentTranslate.examples.append(data)
            self.captureExample = False


class WRClient:

    def __init__(self):
        self.base_url = 'http://www.wordreference.com/fren'
        self.last_words = {}

    def get_url(self, word):
        return '%s/%s' % (self.base_url, word)

    def get_word(self, word):

        if not word:
            raise NoWordGivenException()
            
        if word in self.last_words:
            return self.last_words[word]
            
        url = self.get_url(word)
        resp = requests.get(url)
        
        parser = WRHTMLParser(word)
        parser.feed(resp.text)
         
        translateWord = parser.get_word()
        translateWord.url = url
        
        self.last_words[word] = translateWord
        
        return translateWord


if __name__ == '__main__':
    
    w = WRClient().get_word('pissenlit')
    w.print()
    exit()
