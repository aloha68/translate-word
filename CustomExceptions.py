#!/usr/bin/env python3


class EntryNotFoundException(Exception):
    """
    Exception relevée si aucune entrée n'est trouvée pour un mot
    """
    def __init__(self):
        Exception.__init__(self)


class NoWordGivenException(Exception):
    """
    Exception relevée si le mot recherché est nul
    """
    def __init__(self):
        Exception.__init__(self)
     
